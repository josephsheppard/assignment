# Overview #
The purpose of this assignment is to generate a list of synonyms based on a user’s search query. The user should be presented with an input text box and a search button. After the query is entered you should return a list of synonyms by interacting with Altervista’s API.
Something like this could be included as an add-on for some of our search products so you should focus on making this code as easy to integrate and extend as possible.

### Objectives ###
* Obtain an API key from Altervista’s Thesaurus (http://thesaurus.altervista.org/mykey).
* Create the necessary classes for querying the API and returning results.
* Produce clean and well formatted/documented code.

### Example Search Queries ###
* Environment Course
* Environment Study
* Workplace Management
* Archaeology

### Timescale ###
We recommend that you spend no more than a few hours developing your solution. The main focus is on your coding style and ability.

### Resources ###
* http://thesaurus.altervista.org/service