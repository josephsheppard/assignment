<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title><?php echo (!empty($title) ? $title . ' - ' : ''); ?>PHP Application Assignment</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/main.css">
	<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/vendor/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/main.js"></script>
</head>
<body>
	<header class="container center">
		<img src="<?php echo $baseUrl; ?>/img/TERMINALFOUR-Logo.png">
	</header>
	<section class="container">
		<?php include($view); ?>
	</section>
	<footer class="container center">
		<ul>
			<li>Assignment by Joseph Sheppard</li>
			<li>Email: joseph.sheppard@ymail.com</li>
			<li>Phone: 089 471 0259</li>
		</ul>
	</footer>
</body>
</html>