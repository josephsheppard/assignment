<div class="form center">
	<h1>Generate Synonyms</h1>
	<form method="get">
		<input name="keyword" type="text" placeholder="Enter Keyword" autofocus />
		<input type="submit" />
	</form>
</div>

<div class="results">
	<?php if(isset($synonyms['error'])) : ?>
		<h3 class="center"><?php echo $synonyms['error']; ?></h3>
	<?php else: ?>
		<?php if(!empty($synonyms)) : ?>
			<h2 class="center">Results</h2>
			<?php foreach($synonyms as $keyword => $synonym) : ?>
				<h3><?php echo $keyword; ?></h3>
				<ul>
				<?php foreach($synonym as $word) : ?>
					<li><?php echo $word; ?></li>
				<?php endforeach; ?>
				</ul>
			<?php endforeach; ?>
	<?php endif; ?>
	<?php endif; ?>
</div>