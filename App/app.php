<?php

use \Library\GenerateSynonyms as Synonyms;

class App 
{
	public function __construct($baseUrl = null)
	{
		$this->baseUrl = $this->buildBaseUrl($baseUrl);
	}

    /**
     * Initalises app and synonyms based on _GET param 'keyword'
     */
	public function run()
	{
		$data['title'] = 'Generate Synonyms';
		$data['view'] = 'synonyms_form.php';

		if( isset($_GET['keyword']) ) {
			$synonyms = new Synonyms(API_KEY, 'http://thesaurus.altervista.org/thesaurus/v1', 'en_US');
			$data['synonyms'] = $synonyms->get($_GET['keyword']);
		} else {
			$data['synonyms'] = array();
		}

		$this->loadView($data);
	}

	/**
	 * loades view and extracts $data to variables used in view file.
	 */
	private function loadView($data = array())
	{
		extract($data);
		$baseUrl = $this->baseUrl;
		include('views/template.php');
	}

	/**
	 *	@params - $baseUrl string
	 *  If baseUrl is null, builds baseUrl from _SERVER variables
	 */
	private function buildBaseUrl($baseUrl)
	{
		if($baseUrl == null) {
			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
				return 'https://' . $_SERVER['SERVER_NAME'];
			} else {				
				return 'http://' . $_SERVER['SERVER_NAME'];
			}
		}  

		return $baseUrl;
	}
}