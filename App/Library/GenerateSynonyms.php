<?php 
namespace Library;

use \Library\Request as Request;

class GenerateSynonyms
{
	public $apiKey = '';

	/*
	 * @params $apiKey - string, $endPoint - string, api end point url for generating synonym, $languague - string
	 * 
	 */
	public function __construct($apiKey = null, $endPoint = null, $language = null)
	{
		if($this->checkLanguage($language) === false) {
			return array('status' => 'error','data' => 'Language parameter is not valid');
		}
		$this->apiKey = $apiKey;
		$this->request = new Request($endPoint);
	}

	/*
	 * @params $keyword - string
	 * returns list of synonyms
	 */
	public function get($keyword = null)
	{
		if($keyword == null) {
			return array('error' => 'Please enter a keyword');
		}

		$synonyms = array();
		$synonyms = array_merge($synonyms, $this->generate($keyword));

		$keywords = explode(' ', $keyword);
		if(count($keywords) > 1) {
			foreach ($keywords as $keyword) {
				$synonyms = array_merge($synonyms, $this->generate($keyword));
			}
		}

		return $synonyms;
	}

	/*
	 * @params $keyword - string
	 * calls api via request class
	 * if multiple words in string, request api for each of the words in string
	 * returns list of synonyms
	 */
	private function generate($keyword)
	{

		$synonyms = array();
		$modified_list = array();

		$url = "?word=" . urlencode($keyword)."&language=" . $this->language . "&key=" . $this->apiKey . "&output=json";
		$data = $this->request->get($url);
		
		if(isset($data->data->error)) {
			return array($keyword => array('no results'));
		} 

		if(isset($data->data->response)) {

			foreach($data->data->response as $synonyms_list) {
				$modified_list = array_merge($modified_list, explode('|', $synonyms_list->list->synonyms));
			}

			$synonyms[$keyword] = array_unique($modified_list);

		}

		return array_unique($synonyms);
	}

	/*
	 * @params $language - string
	 * checks if language is supported
	 * returns boolean
	 */
	private function checkLanguage($language)
	{
		$allowed = array('en_US', 'es_ES', 'de_DE', 'fr_FR', 'it_IT');

		if(in_array($language, $allowed)) {
			$this->language = $language;
			return true;
		} 

		return false;
	}
}