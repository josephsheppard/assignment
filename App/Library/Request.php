<?php

namespace Library;

class Request 
{
	public $endPoint = '';

	/**
	 * @params - $endPoint - string, end point of api which is being called 
	 */
	public function __construct($endPoint = null)
	{
		if($endPoint == null || filter_var($endPoint, FILTER_VALIDATE_URL) === false)
		{
			return false;
		}

		$this->endPoint = $endPoint;

	}

	/**
	 * @params - $url - string contains parameters to make curl request
	 */
	public function get($url)
	{
		return $this->curl($url);
	}

	/**
	 * @params - $url - string. $params - array: post data
	 */
	public function post($url, $params = array()) 
	{
		return $this->curl($url, 'post', $params);
	}

	/**
	 * @params - $url - string, $request_type - string: get or post, $params - for post requests
	 * makes curl request
	 */
	private function curl($url = null, $request_type = 'get', $params = null)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_URL, $this->endPoint . $url);

		if($request_type == 'post') {
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		}

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$response = curl_exec($curl);

		curl_close($curl);

		$return  = new \stdClass;
		$return->status = $httpcode;
		$return->data = json_decode($response);

		return $return;
	}
}